'use strict';
let myForm = document.getElementById("myForm");
let labels = [
  "name",
  "position",
  "email",
  "dl",
  "hkm",
  "chm"
]

function metaData(json, str) {
  if (json[str] == "") return "";
  switch (str) {
    case "name":
      return `<span style="font-weight:bold">${json[str]}</span>`;
    case "position":
      return `${json[str]}`;
    case "email":
      return `<a href="mailto:${json[str]}">${json[str]}</a>`;
    case "dl":
      return `D +852 ${json[str]} `;
    case "hkm":
      return `HKM +852 ${json[str]} `;
    case "chm":
      return `CHM +86 ${json[str]} `;
    default:
      return "";
  }
}

function getData(json) {
  for (let label of labels) {
    json[label] = myForm.querySelector(`input[name='${label}']`).value;
  }
}

function render(json) {
  let select = [1, 3, 9, 10, 13, 14];
  let p = document.querySelectorAll("body>div>p");
  for (let i = 0; i < select.length; i++) {
    let id = select[i];
    let str = metaData(json, labels[i]);
    p[id].innerHTML = str;
  }
}

function run() {
  let json = {};
  getData(json);
  render(json);
}

function clearForm() {
  for (let label of labels) {
    myForm.querySelector(`input[name='${label}']`).value = "";
  }
}

function copyToClipboard() {
  console.log("copy?");
  let clipboard = new ClipboardJS('.copy-btn', {
    target: function () {
      return document.querySelector('#copy-target');
    }
  });
  alert('Successfully copied to clipboard!\nYou can now paste it on your signature textarea!');
}
run();