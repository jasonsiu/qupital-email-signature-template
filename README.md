# Qupital Email Signature Template

This Qupital Email Signature Template is a static web page that helps staffs in Qupital to setup proper their e-mail signature properly.

## How to build

```sh
docker image build --tag qupital-email-signature-template .
```

## How to deploy

```sh
docker-compose up -d
```

## How to use

- open [localhost:3905](http://localhost:3905)
- enter your name, position etc
- click "Generate"
- click "Copy"
- go to your g-mail
- Setting -> General -> Signature
- paste it